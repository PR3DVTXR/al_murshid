from django.views.generic.list import ListView
from django.shortcuts import render
from .models import Dua, DuaCategory


def home(request):
    return render(request, 'dua/home.html')



class Search(ListView):

    template_name = 'dua/search.html'
    context_object_name = 'duacategory'
    paginate_by = 5 

    def get_queryset(self):
        return DuaCategory.objects.filter(title__icontains=self.request.GET.get('q'))
    
    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['q'] = self.request.GET.get('q')
        return context