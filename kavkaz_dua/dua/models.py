from django.db import models
from django.contrib.auth.models import User
from django.db.models.query import QuerySet


class DuaCategory(models.Model):

	title = models.CharField(max_length=100)
	quantity = models.CharField(max_length=4)
	func_id = models.CharField(max_length=10)
	chevron_id = models.CharField(max_length=20)
	dua_section_id = models.CharField(max_length=20)

	dua1 = models.CharField(max_length=200)
	dua2 = models.CharField(max_length=200, blank=True)
	dua3 = models.CharField(max_length=200, blank=True)
	dua4 = models.CharField(max_length=200, blank=True)
	dua5 = models.CharField(max_length=200, blank=True)
	dua6 = models.CharField(max_length=200, blank=True)
	dua7 = models.CharField(max_length=200, blank=True)
	dua8 = models.CharField(max_length=200, blank=True)
	dua9 = models.CharField(max_length=200, blank=True)
	dua10 = models.CharField(max_length=200, blank=True)
	dua11 = models.CharField(max_length=200, blank=True)

	link_dua1 = models.CharField(max_length=200)
	link_dua2 = models.CharField(max_length=200, blank=True)
	link_dua3 = models.CharField(max_length=200, blank=True)
	link_dua4 = models.CharField(max_length=200, blank=True)
	link_dua5 = models.CharField(max_length=200, blank=True)
	link_dua6 = models.CharField(max_length=200, blank=True)
	link_dua7 = models.CharField(max_length=200, blank=True)
	link_dua8 = models.CharField(max_length=200, blank=True)
	link_dua9 = models.CharField(max_length=200, blank=True)
	link_dua10 = models.CharField(max_length=200, blank=True)
	link_dua11 = models.CharField(max_length=200, blank=True)

	def __str__(self):
		return self.title
	


class Dua(models.Model):
    class NewManager(models.Manager):
        def get_queryset(self):
            return super().get_queryset()

    arab_text = models.TextField()
    transcription = models.TextField(blank=True)
    traduction = models.TextField()
	
    favorites = models.ManyToManyField(User, related_name='favorite', default=None, blank=True)
    LinkToCategory = models.ForeignKey(DuaCategory, on_delete=models.CASCADE)
    
    objects = models.Manager()
    newmanager = NewManager()