from django.urls import path
from . import views


urlpatterns = [

    
    # Voyage
    path('voyage_1', views.voyage_1, name='voyage_1'),
    path('voyage_2', views.voyage_2, name='voyage_2'),
    path('voyage_3', views.voyage_3, name='voyage_3'),
    path('voyage_4', views.voyage_4, name='voyage_4'),
    path('voyage_5', views.voyage_5, name='voyage_5'),
    path('voyage_6', views.voyage_6, name='voyage_6'),
    path('voyage_7', views.voyage_7, name='voyage_7'),
    path('voyage_8', views.voyage_8, name='voyage_8'),

    # City
    path('city_1', views.city_1, name='city_1'),
    path('city_2', views.city_2, name='city_2'),

    # Vent
    path('vent', views.vent, name='vent'),
]