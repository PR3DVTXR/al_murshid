from django.apps import AppConfig


class PryerConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'pryer'
