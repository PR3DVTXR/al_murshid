from django.shortcuts import render
from bs4 import BeautifulSoup
from datetime import datetime
import requests


def pryer(request):
    # connection to the website

    proxy = {
        "https": "http://51.210.28.182"
    }
    url = "https://www.guidemusulman.com/horaires-prieres/mensuel/ville/25798/clermont-ferrand-63100"
    req = requests.get(url, proxies=proxy) # zaxodim v sait
    soup = BeautifulSoup(req.text, "html.parser") # polutchaem html code stranitsi


    # today timetable
    my_list = soup.find_all("tr", style="background-color:#FFDEB0;")
    conv_st = str(my_list)
    time_t = conv_st.split("<td>")

    # tomorow timetable
    my_list_2 = soup.find_all("table", id="timetable" )
    conv_st_2 = str(my_list_2)
    cut_1 = conv_st_2.split("</td><td>")


    # ready pryer_td
    fajr_nf = str(time_t[1])
    fajr = fajr_nf.split("</td>")
    shuruk_nf = str(time_t[2])
    shuruk = shuruk_nf.split("</td>")
    dhur_nf = str(time_t[3])
    dhur = dhur_nf.split("</td>")
    asr_nf = str(time_t[4])
    asr = asr_nf.split("</td>")
    maghrib_nf = str(time_t[5])
    maghrib = maghrib_nf.split("</td>")
    isha_nf = str(time_t[6])
    isha = isha_nf.split("</td>")

    # ready pryer_tm
    fajr_tm = str(cut_1[7]) #fajr
    shuruk_tm = str(cut_1[8]) #shuruk
    dhur_tm = str(cut_1[9]) #dhur
    asr_tm = str(cut_1[10]) #asr
    maghrib_tm = str(cut_1[11]) #maghrib

    #isha tomorow
    isha_1 = str(cut_1[12])
    isha_2 = isha_1.split("</td></tr><tr")
    isha_tm = str(isha_2[0])


    # Clock
    current_datetime = datetime.now()
    current_time = current_datetime.strftime("%H:%M")


    return render(request, 'pryer/pryer.html', {'fajr':fajr[0], 'shuruk': shuruk[0], 'dhur': dhur[0], 'asr': asr[0], 'maghrib': maghrib[0], 'isha': isha[0], 'time': current_time})
