from django.contrib import admin
from django.urls import path, include
from dua import views as vdua
from pryer import views as vpryer


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', vdua.home, name='home'),
    path('search/', vdua.Search.as_view(), name='search'),
    path('pryer/', vpryer.pryer, name='pryer'),
    path('auth/', include('authentification.urls')),
]