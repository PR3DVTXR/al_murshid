from django.contrib.auth import login, authenticate
from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.db import IntegrityError
from dua import views


home = views.home

def signinuser(request):
    if request.method == 'GET':
        return render(request, 'authentification/signinuser.html')
    else:
        if request.POST['password1'] == request.POST['password2']:
            try:
                MailToName = request.POST['mail']
                splits = MailToName.split('@')
                RealUsername = splits[0]
                user = User.objects.create_user(username=RealUsername, email=request.POST['mail'], password=request.POST['password1'])
                user.save()
                login(request, user)
                return redirect(home)
            except IntegrityError:
                return render(request, 'authentification/signinuser.html', {'error':'Erreur, Cette adresse mail est deja utilise!'})
        else:
            return render(request, 'authentification/signinuser.html', {'error':'Erreur, Mauvaise confirmation du mot de passe!'})



def loginuser(request):
    if request.method == 'GET':
        return render(request, 'app1/loginuser.html')
    else:
        MailToName = request.POST['mail']
        splits = MailToName.split('@')
        RealUsername = splits[0]
        user = authenticate(request, username=request.POST[RealUsername], email=request.POST['mail'], password=request.POST['password'])
        if user is None:
            return render(request, 'authentification/loginuser.html', {'error':'Erreur, Le mail et le mot de passe ne concorde pas!'})
        else:
            login(request, user)
            return redirect(home)
            

def logoutuser(request):
    pass