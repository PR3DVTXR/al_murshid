from django.urls import path
from . import views


urlpatterns = [
    path('signin/', views.signinuser, name='signinuser'),
    path('login/', views.loginuser, name='loginuser'),
    path('logout/', views.logoutuser, name='logoutuser')
]